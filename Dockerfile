FROM ubuntu:14.04.3
MAINTAINER Steve Siadak <swsiadak@gmail.com>

RUN apt-get update
RUN sudo apt-get upgrade

# Install build tools through apt
RUN sudo apt-get install -y vmfs-tools
RUN sudo apt-get install -y gcc
RUN sudo apt-get install -y make
RUN sudo apt-get install -y openssh-server
RUN sudo apt-get install -y pdksh
RUN sudo apt-get install -y lsof
RUN sudo apt-get install -y curl
RUN sudo apt-get install -y git
RUN sudo apt-get install -y aufs-tools
RUN sudo apt-get install -y cmake
#RUN sudo apt-get install -y linux-image-extra-`uname -r`
RUN sudo apt-get install -y libarchive.dev

# need svn 1.6
RUN mv /etc/apt/sources.list /etc/apt/orig.sources.list
COPY precise_sources.list /etc/apt/sources.list
RUN sudo apt-get update
RUN sudo apt-get remove subversion
RUN sudo apt-get install -y subversion
RUN echo subversion hold | sudo dpkg --set-selections
RUN mv /etc/apt/orig.sources.list /etc/apt/sources.list
RUN sudo apt-get update

# chroot build uses pacman
RUN wget https://sources.archlinux.org/other/pacman/pacman-4.0.3.tar.gz -O /tmp/pacman-4.0.3.tar.gz
RUN tar -zxvf /tmp/pacman-4.0.3.tar.gz -C /tmp
RUN cd /tmp/pacman-4.0.3/ \
    && ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
       --disable-doc --without-openssl \
    && make \
    && make install

RUN ln -fs /usr/bin/python /usr/local/bin/python
